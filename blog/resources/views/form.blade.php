<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up From</h2>
	<!-- Form -->
	<form action="/login" method="POST">
		@csrf
		<!-- Input -->
		<label>First Name:</label><br><br>
		<input type="text" name="awal"><br><br>
		<label>Last Name:</label><br><br>
		<input type="text" name="akhir"><br><br>
		<!-- Radio -->
		<label>Gender:</label><br><br>
		<input type="Radio" name="male"><label>Laki-laki</label><br>
		<input type="Radio" name="Female"><label>Perempuan</label><br>
		<input type="Radio" name="Other"><label>Other</label><br><br>
		<!-- Radio -->
		<label>Nationalty:</label><br><br>
		<Select name="Negara">
			<option>Indonesia</option><br>
			<option>English</option><br>
			<option>Others</option><br><br>
		</Select>
		<!-- Checkbox -->
		<label>Languange Spoken:</label><br><br>
		<input type="Checkbox" name="indo"><label>Bahasa Indonesia</label><br>
		<input type="Checkbox" name="eng"><label>English</label><br>
		<input type="Checkbox" name="others"><label>Others</label><br><br>
		<!-- Textarea -->
		<label>Bio:</label><br><br>
		<textarea name="bio" cols="30" rows="10"></textarea><br>
		<input type="submit" name="" value="Sign Up">
	</form>
</body>
</html>